<?php get_header(); ?>

	<div data-ng-controller="ForumsController">
		<div data-ng-bind="FORUMSPAGE"></div>

		<?php
		the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<h1 class="entry-title"><?php the_title(); ?></h1>
			<div class="entry-content">
				<?php the_content(); ?>
			</div>
		</div>

	</div>


<?php get_footer(); ?>